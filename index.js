const express = require('express')
const mongoose = require('mongoose')
const dotenv = require('dotenv')

dotenv.config()

const port = 3001
const app = express()

app.use(express.json())
app.use(express.urlencoded({extended: true}))




mongoose.connect(`mongodb+srv://${process.env.MONGODB_USER}:${process.env.MONGODB_PASSWORD}@cluster0.qk2mc8i.mongodb.net/?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection

db.on('error', () => console.log("Connection error!"))
db.on('open', () => console.log("Connected to MongoDB"))

const userSchema = new mongoose.Schema({
	username: String,
	password: String
})

const User = mongoose.model('User', userSchema)

app.post("/signup", (request, response) => {
	let newUsername = request.body.username
	User.findOne({username: newUsername}, (error, result) => {
		if(result !== null && result.username === newUsername) {
			return response.send(`Username ${newUsername} already exists`)
		}


		let newUser = new User({
			username : newUsername,
			password: request.body.password
		})

		newUser.save((error, savedTask) => {
			if(error){
				return console.error(error)
			} else {
				return response.status(200).send(`Username ${newUsername} successfully created!`)
			}
		})
		
	})
})

app.listen(port, ()=> console.log(`Server online in localhost:${port}`))
